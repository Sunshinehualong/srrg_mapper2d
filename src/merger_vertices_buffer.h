#pragma once

#include "g2o/core/optimizable_graph.h"

namespace srrg_mapper2d{

  using namespace g2o;
  
  class MergerVerticesBuffer {
  public:
    MergerVerticesBuffer();
    
    void init();
    void setClosuresBuffer(OptimizableGraph::EdgeSet& closuresBuffer);
    void addVertices(OptimizableGraph::VertexSet& vertices);
    
    bool getNextVertices(OptimizableGraph::VertexSet& vertices);

    void removeVertex(OptimizableGraph::Vertex* v);
    void removeVertices(OptimizableGraph::VertexSet vertices);
    void clean();

    inline std::vector<OptimizableGraph::VertexSet> vset() {return _vsetToMerge;}
    
  protected:

    size_t _current_it;
    std::vector<OptimizableGraph::VertexSet> _vsetToMerge; //all vertices sets available for merging
    std::vector<bool> _ableToMerge; //mark those vertices that can be merged (not in clousres buffer)
    std::vector<bool> _needClean; //mark those vertices that will be removed
    OptimizableGraph::VertexSet _verticesInClosuresBuffer;

    bool inQueue(OptimizableGraph::VertexSet& vertices);
    void checkFreeVertices();

  };
}
