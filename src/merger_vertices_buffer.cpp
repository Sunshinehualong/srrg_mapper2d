#include "merger_vertices_buffer.h"

namespace srrg_mapper2d{

  MergerVerticesBuffer::MergerVerticesBuffer(){
    _current_it = 0;
    _vsetToMerge.clear();
    _ableToMerge.clear();
    _needClean.clear();
    _verticesInClosuresBuffer.clear();
  }

  void MergerVerticesBuffer::init() {
    //Initial check on available vertices
    std::fill (_needClean.begin(),_needClean.end(), false);
    checkFreeVertices(); //sets current vertices able to be merged
    _current_it = 0;
  }

  void MergerVerticesBuffer::setClosuresBuffer(OptimizableGraph::EdgeSet& closuresBuffer) {
    _verticesInClosuresBuffer.clear();
    for (OptimizableGraph::EdgeSet::iterator it=closuresBuffer.begin(); it!=closuresBuffer.end();it++){
      OptimizableGraph::Edge *e=(OptimizableGraph::Edge*)(*it);
      for (size_t i=0; i<e->vertices().size(); i++){
	OptimizableGraph::Vertex* v=(OptimizableGraph::Vertex*)e->vertices()[i];
	_verticesInClosuresBuffer.insert(v);
      }
    }
  }

  void MergerVerticesBuffer::addVertices(OptimizableGraph::VertexSet& vertices) {
    if (vertices.size() > 1){
      //add vertices to the set of vertices to merge.
      if (!inQueue(vertices)){
	_vsetToMerge.push_back(vertices);
	_ableToMerge.push_back(false);
	_needClean.push_back(false);
      }
    }
  }

  bool MergerVerticesBuffer::getNextVertices(OptimizableGraph::VertexSet& vertices) {
    if (!_vsetToMerge.size()) //no vertices to merge
      return false;

    for (size_t i = _current_it; i < _ableToMerge.size(); i++) {
      if (_ableToMerge[i]){
	_current_it = i;
	vertices = _vsetToMerge[_current_it];
	return true;
      }
    }

    return false;
  }
  
  void MergerVerticesBuffer::removeVertex(OptimizableGraph::Vertex* v) {
    std::cerr << "Removing vertex: " << v->id() << std::endl;
    for (size_t i = 0; i < _vsetToMerge.size(); i++){
      OptimizableGraph::VertexSet& vertices = _vsetToMerge[i];
      OptimizableGraph::VertexSet::iterator itv = vertices.find(v);
      if (itv!=vertices.end()) {
	//remove v from this vertexset
	vertices.erase(itv);
	if (vertices.size()<2){
	  //remove vertices from vsetToMerge (invalidate this set)
	  _ableToMerge[i] = false;
	  _needClean[i] = true;
	}
      }
    }
  }

  void MergerVerticesBuffer::removeVertices(OptimizableGraph::VertexSet vertices){
    for (OptimizableGraph::VertexSet::iterator it=vertices.begin(); it!=vertices.end(); it++){
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*)(*it);
      removeVertex(v);    
    }
  }

  void MergerVerticesBuffer::clean() {
    //remove vertices marked to be cleaned
    assert((_vsetToMerge.size() == _ableToMerge.size()) && "something has been wrongly managed" );
    assert((_vsetToMerge.size() == _needClean.size()) && "something has been wrongly managed" );

    std::vector<bool> needClean = _needClean;
    size_t currenti = 0;
    for (size_t i = 0; i < needClean.size(); i++){
      currenti++;
      if (needClean[i]){
	--currenti;
	_vsetToMerge.erase(_vsetToMerge.begin()+currenti);
	_ableToMerge.erase(_ableToMerge.begin()+currenti);
	_needClean.erase(_needClean.begin()+currenti);
      }
    }
  }

  bool MergerVerticesBuffer::inQueue(OptimizableGraph::VertexSet& vertices) {
    for (size_t i = 0; i < _vsetToMerge.size(); i++) {
      if (vertices == _vsetToMerge[i])
	return true;
    }
    return false;
  }

  void MergerVerticesBuffer::checkFreeVertices() {
    std::cerr << "Vertices pending to merge:" << std::endl;
    for (size_t i = 0; i < _vsetToMerge.size(); i++){
      OptimizableGraph::VertexSet vertices = _vsetToMerge[i];

      for (OptimizableGraph::VertexSet::iterator it=vertices.begin(); it!=vertices.end(); it++){
	std::cerr << " Vertex: " << (*it)->id();
      }
      std::cerr << std::endl;

      _ableToMerge[i] = true;
      for (OptimizableGraph::VertexSet::iterator it=vertices.begin(); it!=vertices.end(); it++){
	OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*)(*it);
	OptimizableGraph::VertexSet::iterator itv = _verticesInClosuresBuffer.find(v);
	if (itv != _verticesInClosuresBuffer.end()) {
	  //Vertex still in closuresBuffer
	  _ableToMerge[i] = false;
	  break;
	}
      }
    }
  }

}
